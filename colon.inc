%define NEXT 0
%macro colon 2
	%2:
	%if NEXT != 0
		dq NEXT
	%else
		dq 0
	%endif
	%define NEXT %2
	db %1, 0
%endmacro
