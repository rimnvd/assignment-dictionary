%include "colon.inc"
%include "lib.inc"
%include "dict.inc"

global _start

section .rodata
%include "words.inc"
input_message: db 'Please, enter the key: ', 0
invalid_key_message: db 'Your key is empty or too long', 0
not_found_message: db 'Nothing found for this key', 0

section .bss
buffer: resb 256

section .text
_start:
	mov rdi, input_message
	mov rsi, 1			
	call print_string					; сообщение о вводе в stdout
	call print_newline_out
	mov rdi, buffer							
	mov rsi, 256
	call read_word						; считываем ключ в буфер из 256 символов
	test rax, rax						; если ключ нулевой или слишком длинный,
	jz .error							; то переходим к метке .error
	mov rdi, buffer						; указатель на введенное значение в  rdi
	mov rsi, NEXT						; указатель на начало словаря в rsi
	call find_word						; ищем ключ введенный ключ в словаре
	test rax, rax						; если в rax 0, то введенный ключ не найден
	jz .not_found						; в таком случае переходи к метке .not_found
	mov rdi, rax						; иначе указатель на значение в словаре в rax
	mov rsi, 1
	call print_string					; печатаем значение в stdout
	call print_newline_out
	xor rdi, rdi
	call exit
	.error:
		mov rdi, invalid_key_message
		mov rsi, 2
		call print_string				; вывод сообщения о невалидном ключе в stderr
		jmp .end
	.not_found:
		mov rdi, not_found_message	
		mov rsi, 2	
		call print_string				; вывод сообщения о том, что введенный ключ не найден, в stderr
		jmp .end
	.end:
		call print_newline_err
		call exit
	
