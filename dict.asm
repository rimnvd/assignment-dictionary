global find_word
%include "lib.inc"

find_word:
	.loop:
		test rsi, rsi			; если указатель на блок нулевой, то мы дошли до конца словаря
		jz .not_found			; выходим из цикла
		push rdi
		push rsi
		add rsi, 8				; перемещаем  указатель на ключ
		call string_equals		; сравниваем текущий ключ и введенное значение
		pop rsi					
		pop rdi
		cmp rax, 1				; если в rax единица, то введенное значение совпало с текущим ключом
		je .found				
		mov rsi, [rsi]			; перемещаем указатель на следующий блок
		jmp .loop				; возвращаемся к началу цикла
	.found:
		add rsi, 9				; получили указатель на ключ (+ 1, чтобы при прибавлении длины ключа сразу получить указатель на начало значения)
		push rsi
		call string_length		; вычислили длину ключа
		pop rsi
		add rax, rsi			; прибавили к указателю длину ключа, результат в rax
		ret
	.not_found:
		mov rax, 0
		ret
	
	
